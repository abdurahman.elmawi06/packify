#!/bin/bash
#This program was designed by Abdurahman Elmawi under the MIT license (Copyright 2022).

source packinfo/vars.sh
modcount=`cat packinfo/mods.list | wc -l`

echo
echo Welcome to the $modpackname modpack installer. This script makes it completely painless to install modpacks to your PC without any hassle [probably].
echo

echo Preparing installer...
echo Detecting Minecraft installation type:
isflatpak=0
if [[ -d ~/.var/app/com.mojang.Minecraft ]]; then
	isflatpak=1
fi

minecraftrootdirectory=~
modpackdirectory=.$modname
if [ $isflatpak == 1 ]; then
	echo Flatpak
	minecraftrootdirectory=~/.var/app/com.mojang.Minecraft
fi
if [ $isflatpak == 0 ]; then
	echo Local
fi
echo

echo The installer is now going to start writing files to the disk.
echo "> $modcount mods"
echo "Target directory: $minecraftrootdirectory/$modpackdirectory"
read -p "Press [ENTER] to accept these changes [or press Ctrl+C to cancel]> "
echo

echo Installing the bundled version of Forge into the Minecraft root directory
cp -r forgefiles/versions $minecraftrootdirectory/.minecraft/
cp -r forgefiles/libraries $minecraftrootdirectory/.minecraft/

echo Installing modpack...
if [[ -d $minecraftrootdirectory/$modpackdirectory ]]; then
	echo
	echo There is already an instance of this modpack installed
	echo The script will now upgrade/reinstall the modpack.
	echo Deleting old mod files...
	rm -rf $minecraftrootdirectory/$modpackdirectory/mods
	rm -rf $minecraftrootdirectory/$modpackdirectory/Flan
fi

echo Copying new mod files..
mkdir -p $minecraftrootdirectory/$modpackdirectory
cp -r modfiles/* $minecraftrootdirectory/$modpackdirectory
echo

echo ---------------------------
echo Installation has completed!
echo
