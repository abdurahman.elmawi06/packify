#!/bin/bash
#This program was designed by Abdurahman Elmawi under the MIT license (Copyright 2022).

source packinfo/vars.sh
modcount=`cat packinfo/mods.list | wc -l`

echo
echo $modpackname server installer

echo Setting installation variables
minecraftrootdirectory=~
modpackdirectory=$modname
echo

echo The installer is now going to start writing files to the disk.
echo "> $modcount mods"
echo "Target directory: $minecraftrootdirectory/$modpackdirectory"
echo Please note that you must have Java installed on the server for it to properly work.
read -p "Press [ENTER] to accept these changes [or press Ctrl+C to cancel] "
echo


echo Installing server...
if [[ -d $minecraftrootdirectory/$modpackdirectory ]]; then
	echo
	echo There is already a server instance detected.
	echo The script will now upgrade/reinstall the modpack.
	echo
	echo - Note: This will not affect current configuration files or any world/player data stored in the directory.
	echo This will only reinstall mod files -
	echo
	echo Deleting old mod files...
	rm -rf $minecraftrootdirectory/$modpackdirectory/mods
	rm -rf $minecraftrootdirectory/$modpackdirectory/Flan
else
	echo
	echo Creating installation directory
	mkdir -p $minecraftrootdirectory/$modpackdirectory
	echo Copying the bundled server configuration into the installation directory...
	cp -r serverfiles/* $minecraftrootdirectory/$modpackdirectory/

fi

echo Copying new mod files...
cp -r modfiles/* $minecraftrootdirectory/$modpackdirectory
echo

echo ---------------------------
echo Installation has completed!
echo To run the server, do [cd $minecraftrootdirectory/$modpackdirectory]
echo From there, do [./run.sh] and the server should start.
echo -- NOTE: The run.sh may use more ram than your sever has. Modify the file according to your server hardware. --
